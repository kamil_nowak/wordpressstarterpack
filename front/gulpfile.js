var elixir = require("laravel-elixir");
var gulp = require("gulp");
require("wo-laravel-elixir-jade");
require("elixir-jshint");
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    //Set template folder name
    var templateName = "templateName";
    /*
     * Root directory 
     */
    elixir.config.assetsPath = ".";
    
    /*
     * BrowserSync config setup
     */
    elixir.config.js.browserify.watchify = {
        enabled: true,
        options: {
            poll: true
        }
    };
    
    /*
     * BrowserSync setup
     */
    mix.browserSync({
        proxy: false,
        server: {
            baseDir: "dist"
        },
        files: [
            "dist/**/*"
        ]
    });

    /*
     * Front assets
     */
    mix.sass("./app/css/main.scss", "./dist/assets/css/main.css");
    mix.sass("./app/css/theme-wpadmin/admin.scss", "./dist/assets/css/admin.css");
    mix.sass("./app/css/theme-wpadmin/login.scss", "./dist/assets/css/login.css");

    mix.scripts([
        "./bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.min.js",
    ], "./dist/assets/js/vendor.js");

    /*
     * Set Js version
     */
    mix.jshint(["app/js/**/*.js"], {
        esversion: 6
    });

    mix.browserify("./app/js/main.js", "./dist/assets/js/main.js");


    mix.jade("./**/*.jade", "dist", {
        basedir: "app/pages",
        extension: ".html"
    });

    /*
     * Copy required fonts and images
     */

    var assetsToCopy = {
        "./dist/assets/css/*.css": "../wp-content/themes/" + templateName + "/assets/css",
        "./dist/assets/js/*.js": "../wp-content/themes/" + templateName + "/assets/js",
        "./dist/assets/img": "../wp-content/themes/" + templateName + "/assets/img",
    }

    for (var directory in assetsToCopy) {
        mix.copy(directory, assetsToCopy[directory]);
    }

});