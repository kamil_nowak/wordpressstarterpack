# README #

## Setup instructions ##

* Make sure you  have npm and nodejs installed, and that you installed bower and gulp globally with npm ( npm install gulp -g; npm install bower -g )
* Open terminal in project's main directory:
* cd front
* open gulpfile and set templateName (it has to be the same as your wordpress theme folder name in wp-content/themes)
* open package.json and set the name of App
* open bower.json and set the name of App
* npm install (if you get any error about writing permission, try with sudo)
* sudo bower install --allow-root
* gulp watch <-- to watch files, gulp <-- to rebuild all files


## Wordpress theme config

* open wp-content/themes/
* rename templateName to your template/project name
* edit style.css and set theme names etc.
* open functions.php and replace 'projectname' with your project name